

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
%matplotlib inline

```


```python
train = pd.read_csv('./data/train.csv', index_col=0)
test = pd.read_csv('./data/test.csv', index_col=0)
```


```python


```


```python
from sklearn.preprocessing import OneHotEncoder
```


```python
all_items = np.unique(train[train.columns[7:]])
new_df_columns = list(train.columns[:7]) + list(all_items[1:])
cnt = 0
train_items_list = {}
for item in all_items[1:]:
    this_item_index = np.zeros(len(train))
    for column in train.columns[7:]:
        # this_item_index[(train[column] == item)] = column[4:]
        this_item_index[(train[column] == item)] += 1
            # print('item : {}, buy_index: {}'.format(item, buy_index))
    train_items_list[item] = this_item_index
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {} %'.format(int(cnt / len(all_items) * 100)), end='')
    # break
```

     99


```python
cnt = 0
test_items_list = {}
for item in all_items[1:]:
    this_item_index = np.zeros(len(test))
    for column in test.columns[7:]:
        # this_item_index[(test[column] == item)] = column[4:]
        this_item_index[(test[column] == item)] += 1
            # print('item : {}, buy_index: {}'.format(item, buy_index))
    test_items_list[item] = this_item_index
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {}'.format(int(cnt / len(all_items) * 100)), end='')
    # break
```

     99


```python
assert train_items_list.keys() == test_items_list.keys()
```


```python
new_features = train_items_list.keys()
```


```python
for val in train_items_list.keys():
    train[val] = train_items_list[val]
for val in test_items_list.keys():
    test[val] = test_items_list[val]
```


```python
train.dropna(inplace=True)

train_X = train.drop(['Is.Back', 'Customer.ID'], axis=1)
test_X = test.drop(['Is.Back', 'Customer.ID'], axis=1)
train_Y = train[['Is.Back']]
test_Y = test[['Is.Back']]

train_X['Date'] = pd.to_datetime(train_X['Date'])
train_X['year'] = train_X['Date'].dt.year
train_X['month'] = train_X['Date'].dt.month
train_X['day'] = train_X['Date'].dt.day
train_X = train_X.drop('Date', axis=1)

test_X['Date'] = pd.to_datetime(test_X['Date'])
test_X['year'] = test_X['Date'].dt.year
test_X['month'] = test_X['Date'].dt.month
test_X['day'] = test_X['Date'].dt.day
test_X = test_X.drop('Date', axis=1)
```


```python
enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
OH_cols_train = pd.DataFrame(enc.fit_transform(train_X))
OH_cols_test = pd.DataFrame(enc.transform(test_X))
```


```python
OH_cols_train.index = train_X.index
OH_cols_test.index = test_X.index
```


```python
# train_X.drop('Country', axis=1, inplace=True)
# test_X.drop('Country', axis=1, inplace=True)
```


```python
print(OH_cols_train.head(1))
```

       0      1      2      3      4      5      6      7      8      9      \
    1    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0   
    
       ...    60611  60612  60613  60614  60615  60616  60617  60618  60619  60620  
    1  ...      0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0    0.0  
    
    [1 rows x 60621 columns]



```python
# print((train[['isback', 'notback', 'Country']]).groupby('Country').sum())
```


```python
# train_X['Date'] = pd.to_datetime(train_X['Date'])
# train_X['year'] = train_X['Date'].dt.year
# train_X['month'] = train_X['Date'].dt.month
# train_X['day'] = train_X['Date'].dt.day
# train_X = train_X.drop('Date', axis=1)
# train_X = train_X[['Total.Quantity', 'total.Price', 'year', 'month', 'day']]
```


```python
# test_X['Date'] = pd.to_datetime(test_X['Date'])
# test_X['year'] = test_X['Date'].dt.year
# test_X['month'] = test_X['Date'].dt.month
# test_X['day'] = test_X['Date'].dt.day
# test_X = test_X.drop('Date', axis=1)
# test_X = test_X[['Total.Quantity', 'total.Price', 'year', 'month', 'day']]
```


```python
print(train_X.head(2))
```

       Total.Quantity  total.Price  year  month  day
    1              -1        -1.25  2010     12    2
    2              85        13.52  2011     10   31



```python
#all features
from sklearn.tree import DecisionTreeClassifier
dt = DecisionTreeClassifier()
dt.fit(OH_cols_train, train_Y)
```




    DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,
                max_features=None, max_leaf_nodes=None,
                min_impurity_decrease=0.0, min_impurity_split=None,
                min_samples_leaf=1, min_samples_split=2,
                min_weight_fraction_leaf=0.0, presort=False, random_state=None,
                splitter='best')




```python
(dt.predict(OH_cols_test).reshape(len(OH_cols_test), 1) == test_Y).sum() / len(test_Y)
```




    Is.Back    0.696305
    dtype: float64




```python
train.columns
```




    Index([   'Customer.ID', 'Total.Quantity',    'total.Price',        'Country',
                     'Date',        'Is.Back',          'item1',          'item2',
                    'item3',          'item4',
           ...
                      90201,            90202,            90204,            90206,
                      90208,            90209,            90210,            90211,
                      90212,            90214],
          dtype='object', length=3229)




```python
# 69%
```
