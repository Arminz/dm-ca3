

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
%matplotlib inline
```


```python
train = pd.read_csv('./data/basic_features/train.csv', index_col=0)
test = pd.read_csv('./data/basic_features/test.csv', index_col=0)
train_X = train.drop('target', axis=1)
test_X = test.drop('target', axis=1)

```


```python
from sklearn.decomposition import PCA
```


```python
pca = PCA(n_components=100)
```


```python
train_pca = pca.fit_transform(train)
test_pca = pca.transform(test)
```


```python
train_pca_df = pd.DataFrame(train_pca)
test_pca_df = pd.DataFrame(test_pca)
```


```python
train_pca_df.to_csv('./data/pca_features/train.csv', index=False)
test_pca_df.to_csv('./data/pca_features/test.csv', index=False)
```
