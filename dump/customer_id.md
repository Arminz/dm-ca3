

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
```


```python
train = pd.read_csv('./data/train.csv', index_col=0)
test = pd.read_csv('./data/test.csv', index_col=0)
```


```python
train[['Customer.ID']]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Customer.ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>16995</td>
    </tr>
    <tr>
      <th>2</th>
      <td>12552</td>
    </tr>
    <tr>
      <th>3</th>
      <td>13192</td>
    </tr>
    <tr>
      <th>4</th>
      <td>16934</td>
    </tr>
    <tr>
      <th>5</th>
      <td>14976</td>
    </tr>
    <tr>
      <th>6</th>
      <td>17937</td>
    </tr>
    <tr>
      <th>7</th>
      <td>17601</td>
    </tr>
    <tr>
      <th>8</th>
      <td>18053</td>
    </tr>
    <tr>
      <th>9</th>
      <td>14089</td>
    </tr>
    <tr>
      <th>10</th>
      <td>15844</td>
    </tr>
    <tr>
      <th>11</th>
      <td>14796</td>
    </tr>
    <tr>
      <th>12</th>
      <td>12540</td>
    </tr>
    <tr>
      <th>13</th>
      <td>13810</td>
    </tr>
    <tr>
      <th>14</th>
      <td>12982</td>
    </tr>
    <tr>
      <th>15</th>
      <td>14411</td>
    </tr>
    <tr>
      <th>16</th>
      <td>16272</td>
    </tr>
    <tr>
      <th>17</th>
      <td>15237</td>
    </tr>
    <tr>
      <th>18</th>
      <td>17865</td>
    </tr>
    <tr>
      <th>19</th>
      <td>15815</td>
    </tr>
    <tr>
      <th>20</th>
      <td>14778</td>
    </tr>
    <tr>
      <th>21</th>
      <td>12812</td>
    </tr>
    <tr>
      <th>22</th>
      <td>13292</td>
    </tr>
    <tr>
      <th>23</th>
      <td>17704</td>
    </tr>
    <tr>
      <th>24</th>
      <td>15904</td>
    </tr>
    <tr>
      <th>25</th>
      <td>17701</td>
    </tr>
    <tr>
      <th>26</th>
      <td>16415</td>
    </tr>
    <tr>
      <th>27</th>
      <td>13418</td>
    </tr>
    <tr>
      <th>28</th>
      <td>18176</td>
    </tr>
    <tr>
      <th>29</th>
      <td>13774</td>
    </tr>
    <tr>
      <th>30</th>
      <td>13804</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>3471</th>
      <td>17480</td>
    </tr>
    <tr>
      <th>3472</th>
      <td>15364</td>
    </tr>
    <tr>
      <th>3473</th>
      <td>14644</td>
    </tr>
    <tr>
      <th>3474</th>
      <td>14782</td>
    </tr>
    <tr>
      <th>3475</th>
      <td>15705</td>
    </tr>
    <tr>
      <th>3476</th>
      <td>15909</td>
    </tr>
    <tr>
      <th>3477</th>
      <td>15843</td>
    </tr>
    <tr>
      <th>3478</th>
      <td>18066</td>
    </tr>
    <tr>
      <th>3479</th>
      <td>15450</td>
    </tr>
    <tr>
      <th>3480</th>
      <td>18276</td>
    </tr>
    <tr>
      <th>3481</th>
      <td>14764</td>
    </tr>
    <tr>
      <th>3482</th>
      <td>12989</td>
    </tr>
    <tr>
      <th>3483</th>
      <td>14259</td>
    </tr>
    <tr>
      <th>3484</th>
      <td>14467</td>
    </tr>
    <tr>
      <th>3485</th>
      <td>15565</td>
    </tr>
    <tr>
      <th>3486</th>
      <td>13157</td>
    </tr>
    <tr>
      <th>3487</th>
      <td>13364</td>
    </tr>
    <tr>
      <th>3488</th>
      <td>15471</td>
    </tr>
    <tr>
      <th>3489</th>
      <td>15426</td>
    </tr>
    <tr>
      <th>3490</th>
      <td>17459</td>
    </tr>
    <tr>
      <th>3491</th>
      <td>17888</td>
    </tr>
    <tr>
      <th>3492</th>
      <td>17230</td>
    </tr>
    <tr>
      <th>3493</th>
      <td>16903</td>
    </tr>
    <tr>
      <th>3494</th>
      <td>15280</td>
    </tr>
    <tr>
      <th>3495</th>
      <td>14344</td>
    </tr>
    <tr>
      <th>3496</th>
      <td>16884</td>
    </tr>
    <tr>
      <th>3497</th>
      <td>15612</td>
    </tr>
    <tr>
      <th>3498</th>
      <td>12586</td>
    </tr>
    <tr>
      <th>3499</th>
      <td>14712</td>
    </tr>
    <tr>
      <th>3500</th>
      <td>13154</td>
    </tr>
  </tbody>
</table>
<p>3500 rows × 1 columns</p>
</div>




```python
test[['Customer.ID']]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Customer.ID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>17848</td>
    </tr>
    <tr>
      <th>2</th>
      <td>16765</td>
    </tr>
    <tr>
      <th>3</th>
      <td>16998</td>
    </tr>
    <tr>
      <th>4</th>
      <td>12571</td>
    </tr>
    <tr>
      <th>5</th>
      <td>16163</td>
    </tr>
    <tr>
      <th>6</th>
      <td>15103</td>
    </tr>
    <tr>
      <th>7</th>
      <td>14096</td>
    </tr>
    <tr>
      <th>8</th>
      <td>12881</td>
    </tr>
    <tr>
      <th>9</th>
      <td>16698</td>
    </tr>
    <tr>
      <th>10</th>
      <td>13705</td>
    </tr>
    <tr>
      <th>11</th>
      <td>13824</td>
    </tr>
    <tr>
      <th>12</th>
      <td>13093</td>
    </tr>
    <tr>
      <th>13</th>
      <td>14038</td>
    </tr>
    <tr>
      <th>14</th>
      <td>12844</td>
    </tr>
    <tr>
      <th>15</th>
      <td>17556</td>
    </tr>
    <tr>
      <th>16</th>
      <td>15228</td>
    </tr>
    <tr>
      <th>17</th>
      <td>15057</td>
    </tr>
    <tr>
      <th>18</th>
      <td>16672</td>
    </tr>
    <tr>
      <th>19</th>
      <td>15045</td>
    </tr>
    <tr>
      <th>20</th>
      <td>13003</td>
    </tr>
    <tr>
      <th>21</th>
      <td>14578</td>
    </tr>
    <tr>
      <th>22</th>
      <td>13516</td>
    </tr>
    <tr>
      <th>23</th>
      <td>18210</td>
    </tr>
    <tr>
      <th>24</th>
      <td>17426</td>
    </tr>
    <tr>
      <th>25</th>
      <td>17347</td>
    </tr>
    <tr>
      <th>26</th>
      <td>15422</td>
    </tr>
    <tr>
      <th>27</th>
      <td>17370</td>
    </tr>
    <tr>
      <th>28</th>
      <td>16504</td>
    </tr>
    <tr>
      <th>29</th>
      <td>17747</td>
    </tr>
    <tr>
      <th>30</th>
      <td>12879</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>837</th>
      <td>16426</td>
    </tr>
    <tr>
      <th>838</th>
      <td>13504</td>
    </tr>
    <tr>
      <th>839</th>
      <td>15569</td>
    </tr>
    <tr>
      <th>840</th>
      <td>12772</td>
    </tr>
    <tr>
      <th>841</th>
      <td>13754</td>
    </tr>
    <tr>
      <th>842</th>
      <td>15502</td>
    </tr>
    <tr>
      <th>843</th>
      <td>15922</td>
    </tr>
    <tr>
      <th>844</th>
      <td>14971</td>
    </tr>
    <tr>
      <th>845</th>
      <td>16627</td>
    </tr>
    <tr>
      <th>846</th>
      <td>17491</td>
    </tr>
    <tr>
      <th>847</th>
      <td>15313</td>
    </tr>
    <tr>
      <th>848</th>
      <td>12646</td>
    </tr>
    <tr>
      <th>849</th>
      <td>14967</td>
    </tr>
    <tr>
      <th>850</th>
      <td>16411</td>
    </tr>
    <tr>
      <th>851</th>
      <td>12947</td>
    </tr>
    <tr>
      <th>852</th>
      <td>17462</td>
    </tr>
    <tr>
      <th>853</th>
      <td>14292</td>
    </tr>
    <tr>
      <th>854</th>
      <td>17670</td>
    </tr>
    <tr>
      <th>855</th>
      <td>17603</td>
    </tr>
    <tr>
      <th>856</th>
      <td>14849</td>
    </tr>
    <tr>
      <th>857</th>
      <td>16696</td>
    </tr>
    <tr>
      <th>858</th>
      <td>14044</td>
    </tr>
    <tr>
      <th>859</th>
      <td>17398</td>
    </tr>
    <tr>
      <th>860</th>
      <td>17460</td>
    </tr>
    <tr>
      <th>861</th>
      <td>14113</td>
    </tr>
    <tr>
      <th>862</th>
      <td>15445</td>
    </tr>
    <tr>
      <th>863</th>
      <td>12371</td>
    </tr>
    <tr>
      <th>864</th>
      <td>17693</td>
    </tr>
    <tr>
      <th>865</th>
      <td>18232</td>
    </tr>
    <tr>
      <th>866</th>
      <td>14962</td>
    </tr>
  </tbody>
</table>
<p>866 rows × 1 columns</p>
</div>




```python
len(train.join(test, 'Customer.ID', how='inner', lsuffix='_'))
```




    0


