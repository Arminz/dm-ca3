

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
%matplotlib inline
```


```python
result = pd.read_csv('./result/basic_2.csv')
result['predict'] = np.where(result['predict'] == 'Yes', 1, 0)
result['Is.Back'] = np.where(result['Is.Back'] == 'Yes', 1, 0)
```


```python
from sklearn.metrics import precision_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
```


```python
precision_score(result['Is.Back'], result['predict'])

```




    0.7815750371471025




```python
accuracy_score(result['Is.Back'], result['predict'])
```




    0.7263279445727483




```python
confusion_matrix(result['Is.Back'], result['predict'])
```




    array([103, 147,  90, 526])




```python
pd.crosstab(result['Is.Back'], result['predict'])
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>predict</th>
      <th>0</th>
      <th>1</th>
    </tr>
    <tr>
      <th>Is.Back</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>103</td>
      <td>147</td>
    </tr>
    <tr>
      <th>1</th>
      <td>90</td>
      <td>526</td>
    </tr>
  </tbody>
</table>
</div>


