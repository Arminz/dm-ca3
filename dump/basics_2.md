

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
```


```python
train = pd.read_csv('./data/train.csv', index_col=0)
test = pd.read_csv('./data/test.csv', index_col=0)
```


```python
train.dropna(inplace=True)

train_X = train.drop(['Is.Back', 'Customer.ID'], axis=1)
test_X = test.drop(['Is.Back', 'Customer.ID'], axis=1)
train_Y = train[['Is.Back']]
test_Y = test[['Is.Back']]
```


```python
from sklearn.tree import DecisionTreeClassifier
```


```python
dt = DecisionTreeClassifier()
```


```python
from sklearn.preprocessing import OneHotEncoder
```


```python
train_X['Date'] = pd.to_datetime(train_X['Date'])
train_X['year'] = train_X['Date'].dt.year
train_X['month'] = train_X['Date'].dt.month
train_X['day'] = train_X['Date'].dt.day
train_X = train_X.drop('Date', axis=1)

test_X['Date'] = pd.to_datetime(test_X['Date'])
test_X['year'] = test_X['Date'].dt.year
test_X['month'] = test_X['Date'].dt.month
test_X['day'] = test_X['Date'].dt.day
test_X = test_X.drop('Date', axis=1)
```


```python
enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
OH_cols_train = pd.DataFrame(enc.fit_transform(train_X))
OH_cols_test = pd.DataFrame(enc.transform(test_X))
```


```python
OH_cols_train.index = train_X.index
OH_cols_test.index = test_X.index
```


```python
# train_X.drop('Country', axis=1, inplace=True)
# test_X.drop('Country', axis=1, inplace=True)
```


```python
print(train_X.head(1))
```

       Total.Quantity  total.Price    item1  item2  item3  item4  item5  item6  \
    1              -1        -1.25  84947.0      0      0      0      0      0   
    
       item7  item8 ...   item188  item189  item190  item191  item192  item193  \
    1      0      0 ...         0        0        0        0        0        0   
    
       item194  year  month  day  
    1        0  2010     12    2  
    
    [1 rows x 199 columns]



```python
# print((train[['isback', 'notback', 'Country']]).groupby('Country').sum())
```


```python
# train_X['Date'] = pd.to_datetime(train_X['Date'])
# train_X['year'] = train_X['Date'].dt.year
# train_X['month'] = train_X['Date'].dt.month
# train_X['day'] = train_X['Date'].dt.day
# train_X = train_X.drop('Date', axis=1)
# train_X = train_X[['Total.Quantity', 'total.Price', 'year', 'month', 'day']]
```


```python
# test_X['Date'] = pd.to_datetime(test_X['Date'])
# test_X['year'] = test_X['Date'].dt.year
# test_X['month'] = test_X['Date'].dt.month
# test_X['day'] = test_X['Date'].dt.day
# test_X = test_X.drop('Date', axis=1)
# test_X = test_X[['Total.Quantity', 'total.Price', 'year', 'month', 'day']]
```


```python
print(train_X.head(2))
```

       Total.Quantity  total.Price  year  month  day
    1              -1        -1.25  2010     12    2
    2              85        13.52  2011     10   31



```python
#all features
dt.fit(OH_cols_train, train_Y)
```




    DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,
                max_features=None, max_leaf_nodes=None,
                min_impurity_decrease=0.0, min_impurity_split=None,
                min_samples_leaf=1, min_samples_split=2,
                min_weight_fraction_leaf=0.0, presort=False, random_state=None,
                splitter='best')




```python
(dt.predict(OH_cols_test).reshape(len(OH_cols_test), 1) == test_Y).sum() / len(test_Y)
```




    Is.Back    0.71709
    dtype: float64




```python
test['predict'] = dt.predict(OH_cols_test).reshape(len(OH_cols_test), 1)
test[['predict', 'Is.Back']].to_csv('./result/basic_2.csv', index=False)
```


```python

```




    (866,)


