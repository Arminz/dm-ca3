

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
%matplotlib inline
```


```python
train = pd.read_csv('./data/basic_features/train.csv', index_col=0)
test = pd.read_csv('./data/basic_features/test.csv', index_col=0)
```


```python
train_X = train.drop('target', axis=1)
train_Y = train[['target']]
test_X = test.drop('target', axis=1)
test_Y = test[['target']]
```


```python
from sklearn.tree import DecisionTreeClassifier
dt = DecisionTreeClassifier()
```


```python
dt.fit(train_X, train_Y)
```




    DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,
                max_features=None, max_leaf_nodes=None,
                min_impurity_decrease=0.0, min_impurity_split=None,
                min_samples_leaf=1, min_samples_split=2,
                min_weight_fraction_leaf=0.0, presort=False, random_state=None,
                splitter='best')




```python
train_error = (dt.predict(train_X).reshape(-1,1) == train_Y).sum() / len(train_Y)
test_error = (dt.predict(test_X).reshape(-1,1) == test_Y).sum() / len(test_Y)
```


```python
[train_error, test_error]
```




    [target    1.0
     dtype: float64, target    0.618938
     dtype: float64]



<h1> Hyper Parameters Tuning </h1>


```python
train_errors = []
cnt = 0
test_errors = []
depths = range(50, 150, 20)
for depth in depths:
    dt = DecisionTreeClassifier(max_depth=depth)
    dt.fit(train_X, train_Y)
    train_error = ((dt.predict(train_X).reshape(-1,1) == train_Y).sum() / len(train_Y))['Is.Back']
    test_error = ((dt.predict(test_X).reshape(-1,1) == test_Y).sum() / len(test_Y))['Is.Back']
    train_errors.append(train_error)
    test_errors.append(test_error)
    cnt = cnt + 1
    print('\r {} %'.format(int((cnt / len(depths)) * 100)), end='')
    
```

     100 %


```python
plt.title('accuracies w.r.t. max depth')
plt.xlabel('max depth')
plt.ylabel('accuracy')
plt.plot(depths, train_errors, label='train accuracy')
plt.plot(depths, test_errors, label='test accuracy')
# plt.xticks(list(range(50, 200, 30)))
plt.ylim(0,1.1)
plt.legend()
```




    <matplotlib.legend.Legend at 0x7faf5d90cc18>




![png](clean_decision_tree_files/clean_decision_tree_9_1.png)



```python
min(train_errors)
```




    0.93855387253501




```python
min(test_errors)
```




    0.7090069284064665


