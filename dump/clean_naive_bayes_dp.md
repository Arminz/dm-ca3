

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
```


```python
train = pd.read_csv('./data/basic_features/train.csv', index_col=0)
test = pd.read_csv('./data/basic_features/test.csv', index_col=0)
```


```python
train_X = train.drop('target', axis=1)
train_Y = train[['target']]
test_X = test.drop('target', axis=1)
test_Y = test[['target']]
```


```python
from sklearn.naive_bayes import GaussianNB
gnb = GaussianNB()
```


```python
gnb.fit(train_X, train_Y)
```

    /usr/local/lib/python3.6/dist-packages/sklearn/utils/validation.py:761: DataConversionWarning: A column-vector y was passed when a 1d array was expected. Please change the shape of y to (n_samples, ), for example using ravel().
      y = column_or_1d(y, warn=True)





    GaussianNB(priors=None, var_smoothing=1e-09)




```python
(gnb.predict(train_X).reshape(-1,1) == train_Y).sum() / len(train_Y)
```




    target    0.735639
    dtype: float64




```python
(gnb.predict(test_X).reshape(-1,1) == test_Y).sum() / len(test_Y)
```




    target    0.727483
    dtype: float64



<b> it is immune to overfit! </b>
