

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
```


```python
train = pd.read_csv('./data/train.csv', index_col=0)
test = pd.read_csv('./data/test.csv', index_col=0)
```


```python
train_X = train.drop(['Is.Back', 'Customer.ID'], axis=1)
test_X = test.drop(['Is.Back', 'Customer.ID'], axis=1)
train_Y = train[['Is.Back']]
test_Y = test[['Is.Back']]
```


```python
from sklearn.tree import DecisionTreeClassifier
```


```python
dt = DecisionTreeClassifier()
```


```python
# dt.fit(train_X, train_Y)
```


```python
print(train[['Is.Back', 'Country']].corr())
train['isback'] = train[['Is.Back']] == 'Yes'
train['notback'] = train[['Is.Back']] == 'No'

print(train.head(1))
print((train[['isback', 'notback', 'Country']]).groupby('Country').sum())
```


```python
train_X = train_X.drop('Country', axis=1)
test_X = test_X.drop('Country', axis=1)
```


```python
train_X['Date'] = pd.to_datetime(train_X['Date'])
train_X['year'] = train_X['Date'].dt.year
train_X['month'] = train_X['Date'].dt.month
train_X['day'] = train_X['Date'].dt.day
train_X = train_X.drop('Date', axis=1)
train_X = train_X[['Total.Quantity', 'total.Price', 'year', 'month', 'day']]
```


```python
test_X['Date'] = pd.to_datetime(test_X['Date'])
test_X['year'] = test_X['Date'].dt.year
test_X['month'] = test_X['Date'].dt.month
test_X['day'] = test_X['Date'].dt.day
test_X = test_X.drop('Date', axis=1)
test_X = test_X[['Total.Quantity', 'total.Price', 'year', 'month', 'day']]
```


```python
print(train_X.head(2))
```

       Total.Quantity  total.Price  year  month  day
    1              -1        -1.25  2010     12    2
    2              85        13.52  2011     10   31



```python
dt.fit(train_X, train_Y)
```




    DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,
                max_features=None, max_leaf_nodes=None,
                min_impurity_decrease=0.0, min_impurity_split=None,
                min_samples_leaf=1, min_samples_split=2,
                min_weight_fraction_leaf=0.0, presort=False, random_state=None,
                splitter='best')




```python
(dt.predict(test_X) == test_Y) #/ len(test_Y)
```

    /home/eta/.local/lib/python3.6/site-packages/ipykernel_launcher.py:1: DeprecationWarning: elementwise comparison failed; this will raise an error in the future.
      """Entry point for launching an IPython kernel.



    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-117-c926ae9d5225> in <module>()
    ----> 1 (dt.predict(test_X) == test_Y) #/ len(test_Y)
    

    /usr/local/lib/python3.6/dist-packages/pandas/core/ops.py in f(self, other)
       1333             res = self._combine_const(other, func,
       1334                                       errors='ignore',
    -> 1335                                       try_cast=False)
       1336             return res.fillna(True).astype(bool)
       1337 


    /usr/local/lib/python3.6/dist-packages/pandas/core/frame.py in _combine_const(self, other, func, errors, try_cast)
       3985         new_data = self._data.eval(func=func, other=other,
       3986                                    errors=errors,
    -> 3987                                    try_cast=try_cast)
       3988         return self._constructor(new_data)
       3989 


    /usr/local/lib/python3.6/dist-packages/pandas/core/internals.py in eval(self, **kwargs)
       3433 
       3434     def eval(self, **kwargs):
    -> 3435         return self.apply('eval', **kwargs)
       3436 
       3437     def quantile(self, **kwargs):


    /usr/local/lib/python3.6/dist-packages/pandas/core/internals.py in apply(self, f, axes, filter, do_integrity_check, consolidate, **kwargs)
       3327 
       3328             kwargs['mgr'] = self
    -> 3329             applied = getattr(b, f)(**kwargs)
       3330             result_blocks = _extend_blocks(applied, result_blocks)
       3331 


    /usr/local/lib/python3.6/dist-packages/pandas/core/internals.py in eval(self, func, other, errors, try_cast, mgr)
       1406 
       1407         result = _block_shape(result, ndim=self.ndim)
    -> 1408         return [self.make_block(result, fastpath=True, )]
       1409 
       1410     def where(self, other, cond, align=True, errors='raise',


    /usr/local/lib/python3.6/dist-packages/pandas/core/internals.py in make_block(self, values, placement, ndim, **kwargs)
        208             ndim = self.ndim
        209 
    --> 210         return make_block(values, placement=placement, ndim=ndim, **kwargs)
        211 
        212     def make_block_scalar(self, values, **kwargs):


    /usr/local/lib/python3.6/dist-packages/pandas/core/internals.py in make_block(values, placement, klass, ndim, dtype, fastpath)
       2955                      placement=placement, dtype=dtype)
       2956 
    -> 2957     return klass(values, ndim=ndim, fastpath=fastpath, placement=placement)
       2958 
       2959 # TODO: flexible with index=None and/or items=None


    /usr/local/lib/python3.6/dist-packages/pandas/core/internals.py in __init__(self, values, placement, ndim, fastpath)
        118             raise ValueError('Wrong number of items passed %d, placement '
        119                              'implies %d' % (len(self.values),
    --> 120                                              len(self.mgr_locs)))
        121 
        122     @property


    ValueError: Wrong number of items passed 866, placement implies 1



```python
(dt.predict(test_X).reshape(866,1) == test_Y).sum() / len(test_Y)
```




    Is.Back    0.65127
    dtype: float64




```python

```




    (866,)


