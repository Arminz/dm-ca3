

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

```


```python
train = pd.read_csv('./data/basic_features/train.csv')
test = pd.read_csv('./data/basic_features/test.csv')
```


```python
item_columns = train.columns[2:196]
item_columns
```




    Index(['item1', 'item2', 'item3', 'item4', 'item5', 'item6', 'item7', 'item8',
           'item9', 'item10',
           ...
           'item185', 'item186', 'item187', 'item188', 'item189', 'item190',
           'item191', 'item192', 'item193', 'item194'],
          dtype='object', length=194)




```python
all_items = np.unique(train[item_columns])
```


```python
len(all_items)

```




    3044




```python
other_columns = list(train.columns[:2]) + list(train.columns[196:])
new_df_columns = other_columns + list(all_items[1:])
```


```python
# new_df_columns
train.columns[2]
```




    'item1'




```python
# old_items_columns
```


```python
cnt = 0
items_list = {}
for item in all_items[1:]:
    this_item_index = np.zeros(len(train))
    for column in item_columns:
        # this_item_index[(train[column] == item)] = column[4:]
        this_item_index[(train[column] == item)] += 1
            # print('item : {}, buy_index: {}'.format(item, buy_index))
    items_list[item] = this_item_index
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {}%'.format(int(cnt / len(all_items) * 100)), end='')
    # break
```

     99%


```python
items_list[21841]#[items_list[21841] > 0]
```




    array([0., 0., 0., ..., 0., 0., 0.])




```python
train_without_old_items = train.drop(item_columns, axis=1)
cnt = 0
for item in items_list:
    cd = pd.DataFrame(items_list[item], columns=[item])
    train_without_old_items = pd.concat([train_without_old_items, cd], axis=1)
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {}%'.format(int(cnt / len(items_list) * 100)), end='')

```

     99%


```python
train_without_old_items.to_csv('./data/advance_features/train.csv', index=False)

```


```python
cnt = 0
items_list = {}
for item in all_items[1:]:
    this_item_index = np.zeros(len(test))
    for column in item_columns:
        # this_item_index[(test[column] == item)] = column[4:]
        this_item_index[(test[column] == item)] += 1
            # print('item : {}, buy_index: {}'.format(item, buy_index))
    items_list[item] = this_item_index
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {}%'.format(int(cnt / len(all_items) * 100)), end='')
    # break
```

     99%


```python
test_without_old_items = test.drop(item_columns, axis=1)
cnt = 0
for item in items_list:
    cd = pd.DataFrame(items_list[item], columns=[item])
    test_without_old_items = pd.concat([test_without_old_items, cd], axis=1)
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {}%'.format(int(cnt / len(items_list) * 100)), end='')

```

     99%


```python
test_without_old_items.to_csv('./data/advance_features/test.csv', index=False)

```
