

```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
```


```python
train = pd.read_csv('./data/train.csv', index_col=0)
test = pd.read_csv('./data/test.csv', index_col=0)
```


```python
train.dropna(inplace=True)

train_X = train.drop(['Is.Back', 'Customer.ID'], axis=1)
test_X = test.drop(['Is.Back', 'Customer.ID'], axis=1)
train_Y = train[['Is.Back']]
test_Y = test[['Is.Back']]
```


```python
from sklearn.tree import DecisionTreeClassifier
```


```python
dt = DecisionTreeClassifier()
```


```python
from sklearn.preprocessing import OneHotEncoder
```


```python
train_X['Date'] = pd.to_datetime(train_X['Date'])
train_X['year'] = train_X['Date'].dt.year
train_X['month'] = train_X['Date'].dt.month
train_X['day'] = train_X['Date'].dt.day
train_X = train_X.drop('Date', axis=1)

test_X['Date'] = pd.to_datetime(test_X['Date'])
test_X['year'] = test_X['Date'].dt.year
test_X['month'] = test_X['Date'].dt.month
test_X['day'] = test_X['Date'].dt.day
test_X = test_X.drop('Date', axis=1)
```


```python
enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
OH_cols_train = pd.DataFrame(enc.fit_transform(train_X))
OH_cols_test = pd.DataFrame(enc.transform(test_X))
```


```python
OH_cols_train.index = train_X.index
OH_cols_test.index = test_X.index
```


```python
print(train_X.head(2))
```

       Total.Quantity  total.Price         Country    item1  item2  item3  item4  \
    1              -1        -1.25  United Kingdom  84947.0      0      0      0   
    2              85        13.52           Italy  84879.0  20685  22692  22898   
    
       item5  item6  item7 ...   item188  item189  item190  item191  item192  \
    1      0      0      0 ...         0        0        0        0        0   
    2  22939  21216  21218 ...         0        0        0        0        0   
    
       item193  item194  year  month  day  
    1        0        0  2010     12    2  
    2        0        0  2011     10   31  
    
    [2 rows x 200 columns]



```python
#all features
dt.fit(OH_cols_train, train_Y)
```




    DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,
                max_features=None, max_leaf_nodes=None,
                min_impurity_decrease=0.0, min_impurity_split=None,
                min_samples_leaf=1, min_samples_split=2,
                min_weight_fraction_leaf=0.0, presort=False, random_state=None,
                splitter='best')




```python
(dt.predict(OH_cols_test).reshape(len(OH_cols_test), 1) == test_Y).sum() / len(test_Y)
```




    Is.Back    0.71709
    dtype: float64




```python
dt.feature_importances_
```




    array([0.        , 0.        , 0.        , ..., 0.00216975, 0.        ,
           0.        ])




```python
from sklearn.feature_selection import f_regression
```


```python
f_regression(OH_cols_train, train_Y)

```

    /usr/local/lib/python3.6/dist-packages/sklearn/utils/validation.py:761: DataConversionWarning: A column-vector y was passed when a 1d array was expected. Please change the shape of y to (n_samples, ), for example using ravel().
      y = column_or_1d(y, warn=True)



    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-17-e5aef0f97505> in <module>()
    ----> 1 f_regression(OH_cols_train, train_Y)
    

    /usr/local/lib/python3.6/dist-packages/sklearn/feature_selection/univariate_selection.py in f_regression(X, y, center)
        284     # need not center X
        285     if center:
    --> 286         y = y - np.mean(y)
        287         if issparse(X):
        288             X_means = X.mean(axis=0).getA1()


    /usr/local/lib/python3.6/dist-packages/numpy/core/fromnumeric.py in mean(a, axis, dtype, out, keepdims)
       3116 
       3117     return _methods._mean(a, axis=axis, dtype=dtype,
    -> 3118                           out=out, **kwargs)
       3119 
       3120 


    /usr/local/lib/python3.6/dist-packages/numpy/core/_methods.py in _mean(a, axis, dtype, out, keepdims)
         85             ret = ret.dtype.type(ret / rcount)
         86     else:
    ---> 87         ret = ret / rcount
         88 
         89     return ret


    TypeError: unsupported operand type(s) for /: 'str' and 'int'



```python
(rfe.predict(OH_cols_test).reshape(len(OH_cols_test), 1) == test_Y).sum() / len(test_Y)
```




    Is.Back    0.711316
    dtype: float64


