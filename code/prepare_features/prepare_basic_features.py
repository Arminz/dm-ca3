
# coding: utf-8

# In[6]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# In[7]:


train = pd.read_csv('./data/train.csv', index_col=0)
test = pd.read_csv('./data/test.csv', index_col=0)


# <h2> Filter Data </h2>

# In[90]:


train.dropna(inplace=True)


# In[91]:


filtered_train = train[train['total.Price'] > 0]
filtered_train = filtered_train[filtered_train['Total.Quantity'] > 0]
print('{}% filtered'.format(len(filtered_train) / len(train)))
# train = filtered_train
# note: filter has bad effects!


# <h2> Scale </h2>

# In[92]:


from sklearn.preprocessing import StandardScaler
sc = StandardScaler(copy=True, with_mean=True, with_std=True)


# In[93]:


SCALE_COLUMNS = ['total.Price', 'Total.Quantity']
train_scale = sc.fit_transform(train[SCALE_COLUMNS])
train[SCALE_COLUMNS] = train_scale
test_scale = sc.transform(test[SCALE_COLUMNS])
test[SCALE_COLUMNS] = test_scale
train.head(2)


# <h2> Date Features </h2>

# In[94]:


train['DateTime'] = pd.to_datetime(train['Date'], format='%Y-%m-%d')
train['dtime'] = train['DateTime'].values.astype(np.int64)
train['target'] = np.where(train['Is.Back'] == 'Yes', 1, 0) 
train['year'] = train['DateTime'].dt.year
train['month'] = train['DateTime'].dt.month
train['day'] = train['DateTime'].dt.day
train['dayofweek'] = train['DateTime'].dt.dayofweek
train['dayofyear'] = train['DateTime'].dt.dayofyear
# train['hourofday'] = train['DateTime'].dt.hour # we don't have hour of day
train['quarter'] = train['DateTime'].dt.quarter
train['semester'] = np.where(train.quarter.isin([1,2]),1,2)
train['weekend'] = np.where(train.dayofweek.isin([0,6]), 1, 0)


# In[95]:


time_features = ['dtime', 'year', 'month', 'day', 'target', 'dayofweek', 'dayofyear', 'quarter', 'semester', 'weekend']
train[time_features].corr()['target']


# In[96]:


test['DateTime'] = pd.to_datetime(test['Date'], format='%Y-%m-%d')
test['dtime'] = test['DateTime'].values.astype(np.int64)
test['target'] = np.where(test['Is.Back'] == 'Yes', 1, 0) 
test['year'] = test['DateTime'].dt.year
test['month'] = test['DateTime'].dt.month
test['day'] = test['DateTime'].dt.day
test['dayofweek'] = test['DateTime'].dt.dayofweek
test['dayofyear'] = test['DateTime'].dt.dayofyear
# test['hourofday'] = test['DateTime'].dt.hour # we don't have hour of day
test['quarter'] = test['DateTime'].dt.quarter
test['semester'] = np.where(test.quarter.isin([1,2]),1,2)
test['weekend'] = np.where(test.dayofweek.isin([0,6]), 1, 0)


# In[97]:


train.drop(['Date', 'DateTime'], inplace=True, axis=1)
test.drop(['Date', 'DateTime'], inplace=True, axis=1)


# <h2> Customer ID </h2>

# In[98]:


train_X = train.drop(['Is.Back', 'Customer.ID'], axis=1)
test_X = test.drop(['Is.Back', 'Customer.ID'], axis=1)
train_Y = train[['Is.Back']]
test_Y = test[['Is.Back']]


# <h2> Country </h2>

# In[99]:


from sklearn.preprocessing import OneHotEncoder
enc = OneHotEncoder(handle_unknown='ignore', sparse=False)
countries_OH = pd.DataFrame(enc.fit_transform(train_X['Country'].values.reshape(-1,1))).add_prefix('country_')
countries_OH.reset_index(drop=True, inplace=True)
train_X.reset_index(drop=True, inplace=True)
OH_cols_train = pd.concat([train_X, countries_OH], axis=1)


countries_OH_test = pd.DataFrame(enc.transform(test_X['Country'].values.reshape(-1,1))).add_prefix('country_')
countries_OH_test.reset_index(drop=True, inplace=True)
test_X.reset_index(drop=True, inplace=True)
OH_cols_test = pd.concat([test_X, countries_OH_test], axis=1)


# In[100]:


OH_cols_train.drop('Country', inplace=True, axis=1)
OH_cols_test.drop('Country', inplace=True, axis=1)


# In[101]:


OH_cols_train.to_csv('./data/basic_features/train.csv', index=False)
OH_cols_test.to_csv('./data/basic_features/test.csv', index=False)

