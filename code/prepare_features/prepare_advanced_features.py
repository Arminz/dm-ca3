
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# In[2]:


train = pd.read_csv('./data/basic_features/train.csv')
test = pd.read_csv('./data/basic_features/test.csv')


# In[3]:


item_columns = train.columns[2:196]


# In[4]:


all_items = np.unique(train[item_columns])


# In[5]:


len(all_items)


# In[6]:


other_columns = list(train.columns[:2]) + list(train.columns[196:])
new_df_columns = other_columns + list(all_items[1:])


# In[7]:


# new_df_columns
train.columns[2]


# In[18]:


# old_items_columns


# In[9]:


cnt = 0
items_list = {}
for item in all_items[1:]:
    this_item_index = np.zeros(len(train))
    for column in item_columns:
        # this_item_index[(train[column] == item)] = column[4:]
        this_item_index[(train[column] == item)] += 1
            # print('item : {}, buy_index: {}'.format(item, buy_index))
    items_list[item] = this_item_index
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {}%'.format(int(cnt / len(all_items) * 100)), end='')
    # break


# In[10]:


items_list[21841]#[items_list[21841] > 0]


# In[11]:


train_without_old_items = train.drop(item_columns, axis=1)
cnt = 0
for item in items_list:
    cd = pd.DataFrame(items_list[item], columns=[item])
    train_without_old_items = pd.concat([train_without_old_items, cd], axis=1)
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {}%'.format(int(cnt / len(items_list) * 100)), end='')


# In[12]:


train_without_old_items.to_csv('./data/advanced_features/train.csv', index=False)


# In[13]:


cnt = 0
items_list = {}
for item in all_items[1:]:
    this_item_index = np.zeros(len(test))
    for column in item_columns:
        # this_item_index[(test[column] == item)] = column[4:]
        this_item_index[(test[column] == item)] += 1
            # print('item : {}, buy_index: {}'.format(item, buy_index))
    items_list[item] = this_item_index
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {}%'.format(int(cnt / len(all_items) * 100)), end='')
    # break


# In[14]:


test_without_old_items = test.drop(item_columns, axis=1)
cnt = 0
for item in items_list:
    cd = pd.DataFrame(items_list[item], columns=[item])
    test_without_old_items = pd.concat([test_without_old_items, cd], axis=1)
    cnt = cnt + 1
    if cnt % 10 == 0:
        print('\r {}%'.format(int(cnt / len(items_list) * 100)), end='')


# In[15]:


test_without_old_items.to_csv('./data/advanced_features/test.csv', index=False)

