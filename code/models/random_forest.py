
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
# get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


FEATURES_SOURCE = 'pca_advanced_features'
train = pd.read_csv('./data/{}/train.csv'.format(FEATURES_SOURCE), index_col=0)
test = pd.read_csv('./data/{}/test.csv'.format(FEATURES_SOURCE), index_col=0)
all_data = train.append(test)


# In[3]:


train_X = train.drop('target', axis=1)
train_Y = train[['target']]
test_X = test.drop('target', axis=1)
test_Y = test[['target']]
all_data_X = all_data.drop('target', axis=1)
all_data_Y = all_data[['target']]


# In[4]:


from sklearn.ensemble import RandomForestClassifier
rf = RandomForestClassifier


# In[5]:


from utils.metrics import get_metrics
from utils.metrics import get_metrics_kfold


# <h3> Hyper Parameters Tuning </h3>

# <h4> max depth </h4> 

# In[6]:


train_errors = []
cnt = 0
test_errors = []
cross_val_scores = []
depths = list(range(1, 10, 1)) + list(range(10, 20, 3)) 
for depth in depths:
    dt = RandomForestClassifier(max_depth=depth, n_estimators=10)
    dt.fit(train_X, train_Y.values.ravel())
    predictions_train = dt.predict(train_X).reshape(-1, 1)
    train_error = get_metrics(train_Y, predictions_train)
    predictions = dt.predict(test_X).reshape(-1, 1)
    test_error = get_metrics(test_Y, predictions)
    cross_val_scores.append(get_metrics_kfold(dt, train_X, train_Y.values.ravel()))
    train_errors.append(train_error)
    test_errors.append(test_error)
    cnt = cnt + 1
    print('\r tuning max depth: {} %'.format(int((cnt / len(depths)) * 100)), end='')
    


# In[8]:


from utils.metrics import VALUE_PARAMETERS

for parameter_i in range(len(VALUE_PARAMETERS)):
    plt.subplot(len(VALUE_PARAMETERS), 1, parameter_i + 1)
    plt.title('{} w.r.t. max depth'.format(VALUE_PARAMETERS[parameter_i]))
    plt.xlabel('max depth')
    plt.ylabel(VALUE_PARAMETERS[parameter_i])
    plt.plot(depths, [train_error[parameter_i] for train_error in train_errors], label='train {}'.format(VALUE_PARAMETERS[parameter_i]))
    plt.plot(depths, [test_error[parameter_i] for test_error in test_errors], label='test {}'.format(VALUE_PARAMETERS[parameter_i]))
    plt.plot(depths, [cross_val_score[parameter_i] for cross_val_score in cross_val_scores], label='kfold {}'.format(VALUE_PARAMETERS[parameter_i]))
    plt.ylim(0,1.1)
    plt.legend()
    plt.subplots_adjust(top=4)


# In[9]:
from utils.metrics import  ALL_PARAMETERS


best_parameter_to_use = 1  # F2
results = []

train_final_errors = [train_error[best_parameter_to_use] for train_error in train_errors]
best_train_error = max(train_final_errors)
best_train_error_index = train_final_errors.index(max(train_final_errors))
results.append(list(train_errors[best_train_error_index]))


test_final_errors = [test_error[best_parameter_to_use] for test_error in test_errors]
best_test_error = max(test_final_errors)
best_test_error_index = test_final_errors.index(max(test_final_errors))
results.append(list(test_errors[best_train_error_index]))

kfold_final_errors = [kfold_error[best_parameter_to_use] for kfold_error in cross_val_scores]
best_kfold_error = max(kfold_final_errors)
best_kfold_error_index = test_final_errors.index(max(test_final_errors))
results.append(list(cross_val_scores[best_train_error_index]))

result = pd.DataFrame(results, columns=ALL_PARAMETERS)
result['data'] = ['train', 'test', 'kfold']
result


# In[10]:


depths[best_kfold_error_index]


# <h3> n_estimators </h3>

# In[11]:


best_depth = depths[best_kfold_error_index]
train_errors = []
cnt = 0
test_errors = []
cross_val_scores = []
n_estimators = list(range(1, 20, 4)) + list(range(20, 70, 5))
for n_estimator in n_estimators:
    dt = RandomForestClassifier(max_depth=best_depth, n_estimators=n_estimator)
    dt.fit(train_X, train_Y.values.ravel())
    predictions_train = dt.predict(train_X).reshape(-1, 1)
    train_error = get_metrics(train_Y, predictions_train)
    predictions = dt.predict(test_X).reshape(-1, 1)
    test_error = get_metrics(test_Y, predictions)
    cross_val_scores.append(get_metrics_kfold(dt, all_data_X, all_data_Y.values.ravel()))
    train_errors.append(train_error)
    test_errors.append(test_error)
    cnt = cnt + 1
    print('\rtuning n_estimators: {} %'.format(int((cnt / len(n_estimators)) * 100)), end='')
    


# In[ ]:


from utils.metrics import VALUE_PARAMETERS

for parameter_i in range(len(VALUE_PARAMETERS)):
    plt.subplot(len(VALUE_PARAMETERS), 1, parameter_i + 1)
    plt.title('{} w.r.t. number of estimators'.format(VALUE_PARAMETERS[parameter_i]))
    plt.xlabel('max depth')
    plt.ylabel(VALUE_PARAMETERS[parameter_i])
    plt.plot(n_estimators, [train_error[parameter_i] for train_error in train_errors], label='train {}'.format(VALUE_PARAMETERS[parameter_i]))
    plt.plot(n_estimators, [test_error[parameter_i] for test_error in test_errors], label='test {}'.format(VALUE_PARAMETERS[parameter_i]))
    plt.plot(n_estimators, [cross_val_score[parameter_i] for cross_val_score in cross_val_scores], label='kfold {}'.format(VALUE_PARAMETERS[parameter_i]))
    plt.ylim(0,1.1)
    plt.legend()
    plt.subplots_adjust(top=4)


# In[ ]:

best_parameter_to_use = 1  # F2
results = []

train_final_errors = [train_error[best_parameter_to_use] for train_error in train_errors]
best_train_error = max(train_final_errors)
best_train_error_index = train_final_errors.index(max(train_final_errors))
results.append(list(train_errors[best_train_error_index]))


test_final_errors = [test_error[best_parameter_to_use] for test_error in test_errors]
best_test_error = max(test_final_errors)
best_test_error_index = test_final_errors.index(max(test_final_errors))
results.append(list(test_errors[best_train_error_index]))

kfold_final_errors = [kfold_error[best_parameter_to_use] for kfold_error in cross_val_scores]
best_kfold_error = max(kfold_final_errors)
best_kfold_error_index = test_final_errors.index(max(test_final_errors))
results.append(list(cross_val_scores[best_train_error_index]))

result = pd.DataFrame(results, columns=ALL_PARAMETERS)
result['data'] = ['train', 'test', 'kfold']
result


# In[ ]:


n_estimators[best_kfold_error_index]


# In[ ]:


name = 'RandomForest_{}.csv'.format(FEATURES_SOURCE)
result.to_csv('./result/models/{}'.format(name), index=False)


# <h1> Redundant </h1>

# In[85]:
#
#
# [max(train_final_errors), depths[train_final_errors.index(max(train_final_errors))]]
#
#
# # In[86]:
#
#
# [max(test_final_errors), depths[test_final_errors.index(max(test_final_errors))]]
#
#
# # In[87]:
#
#
# [max(cross_final_errors), depths[cross_final_errors.index(max(cross_final_errors))]]
#
#
# # In[89]:
#
#
# dt = DecisionTreeClassifier(max_depth=2)
# dt.fit(train_X, train_Y)
# predictions_train = dt.predict(train_X).reshape(-1, 1)
# train_error = get_metrics(train_Y, predictions_train)
# predictions = dt.predict(test_X).reshape(-1, 1)
# test_error = get_metrics(test_Y, predictions)
#
#
# # In[93]:
#
#
# test_error

