
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
# get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


FEATURES_SOURCE = 'advanced_features'
train = pd.read_csv('./data/{}/train.csv'.format(FEATURES_SOURCE), index_col=0)
test = pd.read_csv('./data/{}/test.csv'.format(FEATURES_SOURCE), index_col=0)
all_data = train.append(test)


# In[3]:


train_X = train.drop('target', axis=1)
train_Y = train[['target']]
test_X = test.drop('target', axis=1)
test_Y = test[['target']]
all_data_X = all_data.drop('target', axis=1)
all_data_Y = all_data[['target']]


# In[4]:


from sklearn.naive_bayes import GaussianNB


# In[5]:


from utils.metrics import get_metrics
from utils.metrics import get_metrics_kfold


# In[6]:


train_errors = []
test_errors = []
cross_val_scores = []
nb = GaussianNB()
nb.fit(train_X, train_Y)
predictions_train = nb.predict(train_X).reshape(-1, 1)
train_error = get_metrics(train_Y, predictions_train)
predictions = nb.predict(test_X).reshape(-1, 1)
test_error = get_metrics(test_Y, predictions)
cross_val_scores.append(get_metrics_kfold(nb, train_X, train_Y.values.ravel()))
train_errors.append(train_error)
test_errors.append(test_error)


# In[10]:


from utils.metrics import ALL_PARAMETERS
best_parameter_to_use = 1  # F2
results = []

train_final_errors = [train_error[best_parameter_to_use] for train_error in train_errors]
best_train_error = max(train_final_errors)
best_train_error_index = train_final_errors.index(max(train_final_errors))
results.append(list(train_errors[best_train_error_index]))


test_final_errors = [test_error[best_parameter_to_use] for test_error in test_errors]
best_test_error = max(test_final_errors)
best_test_error_index = test_final_errors.index(max(test_final_errors))
results.append(list(test_errors[best_train_error_index]))

kfold_final_errors = [kfold_error[best_parameter_to_use] for kfold_error in cross_val_scores]
best_kfold_error = max(kfold_final_errors)
best_kfold_error_index = test_final_errors.index(max(test_final_errors))
results.append(list(cross_val_scores[best_train_error_index]))

result = pd.DataFrame(results, columns=ALL_PARAMETERS)
result['data'] = ['train', 'test', 'kfold']
result


# In[ ]:


name = 'NaiveBayes_{}.csv'.format(FEATURES_SOURCE)
result.to_csv('./result/models/{}'.format(name), index=False)


# <h1> Redundant </h1>
#
# # In[85]:
#
#
# [max(train_final_errors), depths[train_final_errors.index(max(train_final_errors))]]
#
#
# # In[86]:
#
#
# [max(test_final_errors), depths[test_final_errors.index(max(test_final_errors))]]
#
#
# # In[87]:
#
#
# [max(cross_final_errors), depths[cross_final_errors.index(max(cross_final_errors))]]
#
#
# # In[89]:
#
#
# dt = DecisionTreeClassifier(max_depth=2)
# dt.fit(train_X, train_Y)
# predictions_train = dt.predict(train_X).reshape(-1, 1)
# train_error = get_metrics(train_Y, predictions_train)
# predictions = dt.predict(test_X).reshape(-1, 1)
# test_error = get_metrics(test_Y, predictions)
#
#
# # In[93]:
#
#
# test_error
#
