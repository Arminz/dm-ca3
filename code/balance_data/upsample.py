
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# In[2]:


FEATURES_SOURCE = 'pca_advanced_features'
train = pd.read_csv('./data/{}/train.csv'.format(FEATURES_SOURCE), index_col=0)


# In[3]:


minority = train[train['target'] == 0]
majority = train[train['target'] == 1]


# In[4]:


from sklearn.utils import resample
minority_up_sampled = resample(minority, replace=True, n_samples=len(majority) - len(minority), random_state=123)


# In[5]:


minority = pd.concat([minority, minority_up_sampled])


# In[6]:


len(minority), len(minority)


# In[7]:


train = pd.concat([minority, majority])


# In[8]:


train.to_csv('./data/upsampled/train.csv')

