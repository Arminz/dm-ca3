
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# In[2]:


FEATURES_SOURCE = 'pca_advanced_features'
train = pd.read_csv('./data/{}/train.csv'.format(FEATURES_SOURCE), index_col=0)


# In[3]:


minority = train[train['target'] == 0]
majority = train[train['target'] == 1]


# In[4]:


from sklearn.utils import resample
majority_down_sampled = resample(majority, replace=False, n_samples=len(minority), random_state=123)


# In[5]:


len(minority), len(minority)


# In[13]:


train = pd.concat([majority, minority])


# In[15]:


train.to_csv('./data/downsampled/train.csv')


# In[17]:


(train['target'] == 0)

