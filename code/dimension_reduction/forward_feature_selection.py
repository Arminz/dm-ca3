
# coding: utf-8

# In[87]:


import pandas as pd
import matplotlib.pyplot as plt


# In[89]:


FEATURES_SOURCE = 'pca_advanced_features'
train = pd.read_csv('./data/{}/train.csv'.format(FEATURES_SOURCE), index_col=0)
test = pd.read_csv('./data/{}/test.csv'.format(FEATURES_SOURCE), index_col=0)
all_data = train.append(test)


# In[90]:


train_X = train.drop('target', axis=1)
train_Y = train[['target']]
test_X = test.drop('target', axis=1)
test_Y = test[['target']]
all_data_X = all_data.drop('target', axis=1)
all_data_Y = all_data[['target']]


# In[126]:


from sklearn.ensemble import RandomForestClassifier

model = RandomForestClassifier(max_depth=19, n_estimators=30)
algorithm = type(model).__name__

# from sklearn.tree import DecisionTreeClassifier
# model = DecisionTreeClassifier(max_depth=19)
# algorithm = type(model).__name__


# In[127]:


from sklearn.feature_selection import SelectFromModel


# In[128]:


from utils.metrics import get_metrics_kfold, get_metrics
from utils.metrics import ALL_PARAMETERS

thresholds = [0.005, 0.01, 0.02, 0.03]
best_result = 0
parameter = 1 # F2
for threshold in thresholds:
    selector = SelectFromModel(model, threshold=threshold)
    selector.fit(train_X, train_Y.values.ravel())
    pruned_train_X = selector.transform(train_X)
    pruned_test_X = selector.transform(test_X)
    model.fit(pruned_train_X, train_Y.values.ravel())
    predictions_train = model.predict(pruned_train_X)
    predictions_test = model.predict(pruned_test_X)
    result_train = get_metrics(train_Y, predictions_train)
    result_test = get_metrics(test_Y, predictions_test)
    result_kfold = get_metrics_kfold(model, pruned_train_X, train_Y.values.ravel())
    if result_test[parameter] > best_result:
        best_result = result_test[parameter]
        best_threshold = threshold
        result = pd.DataFrame([result_train, result_test, result_kfold], columns=ALL_PARAMETERS)
    result['data'] = ['train', 'test', 'kfold']
    


# In[129]:


result


# In[130]:


model.fit(train_X, train_Y.values.ravel())
predictions_train = model.predict(train_X)
predictions_test = model.predict(test_X)
result_train = get_metrics(train_Y, predictions_train)
result_test = get_metrics(test_Y, predictions_test)
result_kfold = get_metrics_kfold(model, train_X, train_Y.values.ravel())
result = pd.DataFrame([result_train, result_test, result_kfold])
default_result = pd.DataFrame([result_train, result_test, result_kfold], columns=ALL_PARAMETERS)
default_result['data'] = ['train', 'test', 'kfold']
default_result


# In[134]:


name = '{}_forward_feature_selection_{}.csv'.format(algorithm, FEATURES_SOURCE)
result.to_csv('./result/dimension_reduction/{}'.format(name), index=False)


# In[135]:


name = '{}_forward_feature_selection_{}_default.csv'.format(algorithm, FEATURES_SOURCE)
default_result.to_csv('./result/dimension_reduction/{}'.format(name), index=False)


# In[133]:


best_threshold


# <h2> Redundant </h2>

# In[51]:


model.fit(train_X, train_Y)
prediction = model.predict(test_X)
(test_Y.values == prediction).sum() / len(prediction)


# In[17]:


(test_Y['target'] == prediction).sum() / len((test_Y['target']))


# In[56]:


tp = ((test_Y['target'] == prediction) & test_Y['target']).sum()


# In[57]:


tn = ((test_Y['target'] == prediction) & ~test_Y['target']).sum()


# In[58]:


fn = ((test_Y['target'] != prediction) & test_Y['target']).sum()


# In[59]:


fp = ((test_Y['target'] != prediction) & ~test_Y['target']).sum()


# In[60]:


tp / (fn + tp)


# In[61]:


tp / (fp + tp)


# In[62]:


(tp + tn) / (tp + fp + tn + fn)

