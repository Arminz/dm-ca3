from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.metrics.scorer import accuracy_score, recall_score, precision_score
from sklearn.metrics import make_scorer
from sklearn.metrics import confusion_matrix


def get_metrics(real, prediction):
    precision = precision_score(real, prediction)
    recall = recall_score(real, prediction)
    F = (2 * precision * recall) / (precision + recall)
    accuracy = accuracy_score(real, prediction)
    confusion_result = confusion_matrix(real, prediction)
    return accuracy, F, precision, recall, confusion_result


def get_metrics_kfold(model, data_x, data_y, cv=5):
    recall = cross_val_score(model, data_x, data_y, cv=cv, scoring=make_scorer(recall_score)).mean()
    precision = cross_val_score(model, data_x, data_y, cv=cv, scoring=make_scorer(precision_score)).mean()
    accuracy = cross_val_score(model, data_x, data_y, cv=cv, scoring=make_scorer(accuracy_score)).mean()
    F = (2 * precision * recall) / (precision + recall)
    confusion_result = confusion_matrix(data_y, cross_val_predict(model, data_x, data_y, cv=5))
    return accuracy, F, precision, recall, confusion_result


VALUE_PARAMETERS = ['accuracy', 'F2', 'precision', 'recall']
ALL_PARAMETERS = ['accuracy', 'F2', 'precision', 'recall', 'confusion_matrix']