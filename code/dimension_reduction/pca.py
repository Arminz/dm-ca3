
# coding: utf-8

# In[8]:


import pandas as pd
import matplotlib.pyplot as plt


# In[2]:


BASE_SOURCE = 'advanced_features'
train = pd.read_csv('./data/{}/train.csv'.format(BASE_SOURCE), index_col=0)
test = pd.read_csv('./data/{}/test.csv'.format(BASE_SOURCE), index_col=0)
train_Y = train['target']
train_X = train.drop('target', axis=1)
test_Y = test['target']
test_X = test.drop('target', axis=1)


# In[3]:


from sklearn.decomposition import PCA
pca = PCA(n_components=100)


# In[4]:


train_pca = pca.fit_transform(train)
test_pca = pca.transform(test)


# In[5]:


train_pca_df = pd.DataFrame(train_pca)
train_pca_df['target'] = train_Y.values
test_pca_df = pd.DataFrame(test_pca)
test_pca_df['target'] = test_Y.values


# In[7]:


train_pca_df.to_csv('./data/pca_{}/train.csv'.format(BASE_SOURCE), index=False)
test_pca_df.to_csv('./data/pca_{}/test.csv'.format(BASE_SOURCE), index=False)

