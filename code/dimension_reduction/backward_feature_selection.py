
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt


# In[2]:


FEATURES_SOURCE = 'pca_advanced_features'
train = pd.read_csv('./data/{}/train.csv'.format(FEATURES_SOURCE), index_col=0)
test = pd.read_csv('./data/{}/test.csv'.format(FEATURES_SOURCE), index_col=0)
all_data = train.append(test)


# In[5]:


train_X = train.drop('target', axis=1)
train_Y = train[['target']]
test_X = test.drop('target', axis=1)
test_Y = test[['target']]
all_data_X = all_data.drop('target', axis=1)
all_data_Y = all_data[['target']]


# In[13]:


from sklearn.ensemble import RandomForestClassifier
model = RandomForestClassifier(max_depth=19, n_estimators=30)

# from sklearn.tree import DecisionTreeClassifier
# model = DecisionTreeClassifier(max_depth=19)


# In[14]:


algorithm = type(model).__name__


# In[15]:


from sklearn.feature_selection import RFE


# In[16]:


rfe = RFE(model, step=5)
rfe.fit(train_X, train_Y.values.ravel())


# In[18]:


from utils.metrics import get_metrics_kfold, get_metrics
predictions_train = rfe.predict(train_X)
predictions_test = rfe.predict(test_X)
result_train = get_metrics(train_Y, predictions_train)
result_test = get_metrics(test_Y, predictions_test)
result_kfold = get_metrics_kfold(rfe, train_X, train_Y.values.ravel())


# In[20]:


from utils.metrics import ALL_PARAMETERS
result = pd.DataFrame([result_train, result_test, result_kfold], columns=ALL_PARAMETERS)
result['data'] = ['train', 'test', 'kfold']
result


# In[29]:


name = '{}_RFE_{}.csv'.format(algorithm, FEATURES_SOURCE)
result.to_csv('./result/dimension_reduction/{}'.format(name), index=False)


# In[25]:


from utils.metrics import get_metrics_kfold, get_metrics
model.fit(train_X, train_Y.values.ravel())
predictions_train = model.predict(train_X)
predictions_test = model.predict(test_X)
result_train = get_metrics(train_Y, predictions_train)
result_test = get_metrics(test_Y, predictions_test)
result_kfold = get_metrics_kfold(model, train_X, train_Y.values.ravel())


# In[26]:


default_result = pd.DataFrame([result_train, result_test, result_kfold], columns=ALL_PARAMETERS)
default_result['data'] = ['train', 'test', 'kfold']
default_result


# In[28]:


name = '{}_RFE_{}_default.csv'.format(algorithm, FEATURES_SOURCE)
default_result.to_csv('./result/dimension_reduction/{}'.format(name), index=False)

